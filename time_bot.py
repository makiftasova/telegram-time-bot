#! /usr/bin/env python3

"""
    time_bot.py: A simple telegram bot to keep track of time.
    Copyright (C) 2019 Mehmet Akif Tasova

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

__author__ = "Mehmet Akif Tasova"
__copyright__ = "Copyright 2019, Mehmet Akif Tasova"
__license__ = "AGPLv3"
__version__ = "1.0.0"
__maintainer__ = "Mehmet Akif Tasova"
__email__ = "makiftasova@gmail.com"
__status__ = "Production"
__git_url__ = "https://gitlab.com/makiftasova/telegram-time-bot"


import os
import sys
import shutil
import datetime
import appdirs
import json
import logging

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

APP_NAME = "telegram_time_bot"


class Memory:
    _mem_file = ""
    _mem = {}
    _always_sync = False
    _logger = None

    def __init__(self, mem_dir, always_sync=True, immediate_sync=False):
        self._mem_file = f"{mem_dir}/memory.json"
        self._always_sync = always_sync
        if immediate_sync:
            self.sync_from_disk()

    def get_file_path(self):
        return self._mem_file

    def sync_to_disk(self):
        with open(self._mem_file, "w") as f:
            json.dump(self._mem, f)

    def sync_from_disk(self):
        with open(self._mem_file, "r") as f:
            self._mem = json.load(f)

    def add(self, key, value):
        self._mem[key] = value
        if self._always_sync:
            self.sync_to_disk()

    def add_date(self, key, value):
        if isinstance(value, datetime.datetime):
            v = value.strftime("%Y-%m-%d %H:%M:%S")
        else:
            v = value
        self.add(key, v)

    def get(self, key):
        if key in self._mem:
            return self._mem[key]
        return None

    def get_date(self, key):
        v = self.get(key)
        if v:
            try:
                value = datetime.datetime.strptime(v, "%Y-%m-%d %H:%M:%S")
            except ValueError as ex:
                self.logger.warning(
                    f"failed to parse \"{v}\" as datetime object. error: {ex}")
                return None
            return value
        else:
            return None

    def remove(self, key):
        val = None
        if key in self._mem:
            val = self._mem.pop(key)
            self.sync_to_disk()
        return val

    def remove_date(self, key):
        v = self.get_date(key)
        if v:
            self.remove(key)
            return v
        else:
            return None


class Config:
    _config_file = ""
    _config = {}
    _memory = None
    _logger = None

    def __init__(self, logger, memory, config_dir, skip_setup=False):
        self._config_file = f"{config_dir}/config.json"
        self._memory = memory
        self._logger = logger
        if skip_setup:
            self.update()

    def update(self):
        with open(self._config_file, "r") as f:
            self._config = json.load(f)
        self._memory.sync_from_disk()

    def get_file_path(self):
        return self._config_file

    def get_section(self, name):
        return self._config[name]

    def get_memory(self):
        return self._memory


class Proxy:
    KEY = "proxy"
    KEY_USER = "username"
    KEY_PASS = "password"
    KEY_URL = "proxy_url"
    KEY_SCHEMA = "proxy_schema"
    _proxy = {}

    def __init__(self, config: Config):
        self._proxy = config.get_section(self.KEY)

    def __str__(self):
        return self.get_proxy_uri()

    def get_proxy(self):
        return self._proxy

    def get_username(self):
        return self._proxy[self.KEY_USER]

    def get_password(self):
        return self._proxy[self.KEY_PASS]

    def get_proxy_uri(self):
        return "{schema}://{url}".format(schema=self._proxy[self.KEY_SCHEMA],
                                         url=self._proxy[self.KEY_URL])


TIMES = None


def config_init(logger):
    global TIMES
    appdir = appdirs.AppDirs(appname=APP_NAME, appauthor=__author__)
    memory = Memory(appdir.user_config_dir)
    config = Config(logger, memory, appdir.user_config_dir)

    if not os.path.isdir(appdir.user_config_dir):
        try:
            os.makedirs(appdir.user_config_dir)
        except OsError as ex:
            sys.exit(f"Failed to create directory: {ex}")

    if not os.path.isfile(config.get_file_path()):
        try:
            shutil.copy(os.path.abspath(os.path.join(os.path.dirname(
                sys.argv[0]), 'config.json')), config.get_file_path())
        except (OSError, IOError) as ex:
            sys.exit(
                f"Failed to copy default config file: {ex}")

    if not os.path.isfile(memory.get_file_path()):
        memory.sync_to_disk()

    config.update()
    TIMES = config.get_memory()
    return config


def bot_init_updater(config: Config):
    proxy = Proxy(config)
    TOKEN = config.get_section("telegram_token")
    REQUEST_KWARGS = {"proxy_url": proxy.get_proxy_uri()}

    updater = Updater(TOKEN, use_context=True, request_kwargs=REQUEST_KWARGS)

    return updater


def get_command_user_id(update, context):
    return str(update.effective_user.id)


def get_command_user_firstname(update, context):
    return update.effective_user.first_name


def get_command_chat_id(update, context):
    return str(update.effective_chat.id)


def get_command_message(update, context):
    if update.edited_message:
        return update.edited_message, update.edited_message.edit_date
    if update.message:
        return update.message, update.message.date
    if update.edited_channel_post:
        return update.edited_channel_post, update.edited_channel_post.edit_date
    return update.channel_post, update.channel_post.date


def calculate_time_diff(tbegin, tend):
    return tend - tbegin


def cmd_start(update, context):
    uid = get_command_user_id(update, context)
    cid = get_command_chat_id(update, context)
    first_name = get_command_user_firstname(update, context)
    message, date = get_command_message(update, context)
    logger.info(f"User {uid} [{first_name}] joined. cid={cid}")
    message.reply_text(f"Hello {first_name}!")


def cmd_time_set(update, context):
    global TIMES
    uid = get_command_user_id(update, context)
    cid = get_command_chat_id(update, context)
    message, date = get_command_message(update, context)
    logger.info(f"uid={uid} cid={cid} command=\"time_set\"")
    offset = 0
    if len(context.args) > 0:
         try:
             offset = int(context.args[0])
         except (IndexError, ValueError) as ex:
             message.reply_text("usage: /time_set [offset minutes]")
             return
    if offset:
        date -= datetime.timedelta(minutes=offset)
    TIMES.add_date(cid, date)
    message.reply_text("time set!")


def cmd_time_status(update, context):
    global TIMES
    uid = get_command_user_id(update, context)
    cid = get_command_chat_id(update, context)
    message, date = get_command_message(update, context)
    logger.info(f"uid={uid} cid={cid} command=\"time_status\"")
    tbegin = TIMES.get_date(cid)
    if tbegin:
        timediff = date - tbegin
        resp = f"{timediff} passed since time set!"
    else:
        resp = "time is not set!"
    message.reply_text(resp)


def cmd_time_del(update, context):
    global TIMES
    uid = get_command_user_id(update, context)
    cid = get_command_chat_id(update, context)
    message, date = get_command_message(update, context)
    logger.info(f"uid={uid} cid={cid} command=\"time_del\"")
    tbegin = TIMES.remove_date(cid)
    if tbegin:
        timediff = date - tbegin
        resp = f"{timediff} passed. time deleted!"
    else:
        resp = "no time set!"
    message.reply_text(resp)


def cmd_about(update, context):
    resp = f"""
This bot is licensed under {__license__}.
You can find the source code at {__git_url__}
    """
    uid = get_command_user_id(update, context)
    cid = get_command_chat_id(update, context)
    logger.info(f"uid={uid} cid={cid} command=\"about\"")
    message, date = get_command_message(update, context)
    message.reply_text(resp)


def cmd_error(update, context):
    err = context.error
    logger.warning(f"Update \"{update}\" caused error \"{err}\"")


def bot_register_handlers(updater: Updater):
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", cmd_start))
    dp.add_handler(CommandHandler("time_set", cmd_time_set, pass_args=True))
    dp.add_handler(CommandHandler("time_status", cmd_time_status))
    dp.add_handler(CommandHandler("time_del", cmd_time_del))
    dp.add_handler(CommandHandler("about", cmd_about))

    dp.add_error_handler(cmd_error)

    return updater


if "__main__" == __name__:
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)
    logger = logging.getLogger(__name__)
    config = config_init(logger)
    updater = bot_init_updater(config)
    updater = bot_register_handlers(updater)
    updater.start_polling()
    logger.info("Bot started")
    updater.idle()
