
if [ "$#" -ne "2" ]; then
	echo "Please run as $0 <install path> <user to run>"
	exit 1
fi

INSTALL_DIR="$1"
RUN_AS="$2"

INSTALL_PATH="$INSTALL_DIR/time_bot.py"

cp -f time_bot.py "$INSTALL_PATH"

echo "
[Unit]
Description=Telegram Time Bot
After=multi-user.target

[Service]
Type=simple
ExecStart=/usr/bin/python3 $INSTALL_PATH
User=$RUN_AS
Group=$(groups $RUN_AS | cut -d":" -f2 | awk '{print $1}')

[Install]
WantedBy=multi-user.target
" > telegram-time-bot.service

cp -f telegram-time-bot.service /lib/systemd/system/.
