# Install Dependencies
```pip install -r requirements.txt```

# Run bot
```python3 time_bot.py```

# Notes
* Requires Python3.6+
* On first run, Bot will copy dummy config file to `$HOME/.config/time_bot`.
First modify `$HOME/.config/time_bot/config.json` then restart the bot.
